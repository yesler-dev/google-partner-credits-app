package com.possible.google.partnercredits.servlet;

import java.io.BufferedWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.possible.google.partnercredits.PropertiesUtil;
import com.possible.google.partnercredits.entity.Coupon;
import com.possible.google.partnercredits.entity.CouponRecord;

@SuppressWarnings("serial")
public class ExportCouponsServlet extends HttpServlet {

	private static Logger log = Logger.getLogger(ExportCouponsServlet.class.getName());
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		PropertiesUtil props = new PropertiesUtil();
		String token = req.getParameter("token");
		String action = req.getParameter("action");

		if (token == null || (token != null && !token.equals(props.getProperty("security.token")))) {
			//Display the error message
			log.severe("Invalid token. " + token);
			resp.setContentType("text/plain");
			resp.getWriter().println("{\"status\":\"Invalid Token\"} ");
		} else {
			if (action != null && action.trim().equals("excel")) {
				defaultAction(req, resp);
			} else if (action != null && action.trim().equals("html")) {
				list(req, resp);
			}
		}
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doPost(req, resp);
	}

	private void defaultAction(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		// RETRIEVE
		Key<CouponRecord> record = Key.create(CouponRecord.class, "coupon");
		List<Coupon> records = ObjectifyService.ofy().load().type(Coupon.class)
									.ancestor(record)
									// .order("-isAvailable")
									.list();

		// DISPLAY
		resp.setContentType("text/csv");
		resp.setHeader("Content-Disposition",
				"attachment;filename=\"assigned_coupons.csv\"");
		BufferedWriter writer = new BufferedWriter(resp.getWriter());
		String str = "";
		writer.append("Coupon Code");
		writer.append(",");
		writer.append("Requested On");
		writer.append(",");
		writer.append("Claimed On");
		writer.append(",");
		writer.append("Create Timestamp");
		writer.append(",");
		writer.append("Update Timestamp");
		writer.newLine();

		if (!records.isEmpty()) {
			for (Coupon coupon : records) {
				writer.append(coupon.thisCode);
				writer.append(",");
				writer.append(formatDate(coupon.requested_on));
				writer.append(",");
				writer.append(formatDate(coupon.claimed_on));
				writer.append(",");
				writer.append(formatDate(coupon.date));
				writer.append(",");
				writer.append(formatDate(coupon.updated_on));
				writer.newLine();
			}
		}

		writer.flush();
		writer.close();
		
		log.info("CSV Exported.");

	}

	private void list(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		// RETRIEVE
		Key<CouponRecord> record = Key.create(CouponRecord.class, "coupon");
		List<Coupon> records = ObjectifyService.ofy().load().type(Coupon.class)
								.ancestor(record) 
								.list();

		// DISPLAY
		resp.setContentType("text/plain");
		resp.getWriter().println(
				"Coupon Code" + "\t\t   " + "Requested On" + "\t\t   " + "Claimed On"
						+ "\t\t   " + "Create Timestamp" + "\t   "
						+ "Update Timestamp");
		if (!records.isEmpty()) {
			for (Coupon coupon : records) {
				resp.getWriter().println(
						coupon.thisCode + "\t   "
								+ formatDate(coupon.requested_on) + "\t   "
								+ formatDate(coupon.claimed_on) + "\t   "
								+ formatDate(coupon.date) + "\t   "
								+ formatDate(coupon.updated_on));
			}
		}
		
		log.info("Exported as HTML");
	}
	
	private String formatDate(Date date) {
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

		String strDate = "";
		try {
			strDate = df.format(date);
		} catch (Exception e) {
			e.getMessage();
		}

		return strDate;
	}
}