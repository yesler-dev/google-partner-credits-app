package com.possible.google.partnercredits.servlet;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.possible.google.partnercredits.PropertiesUtil;
import com.possible.google.partnercredits.entity.PartnerData;

@SuppressWarnings("serial")
public class PartnerDeleteServlet extends HttpServlet {

	private static Logger log = Logger.getLogger(PartnerDeleteServlet.class.getName());
	
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
				PropertiesUtil props = new PropertiesUtil();
		String token = req.getParameter("token");
		
		if (token == null || (token != null && !token.equals(props.getProperty("security.token")))) {
			//Display the error message
			log.severe("Invalid token. " + token);
			resp.setContentType("text/plain");
			resp.getWriter().println("{\"status\":\"Invalid Token\"}");
		} else {
			List<Key<PartnerData>> partnerkeys = ObjectifyService.ofy().load().type(PartnerData.class).keys().list();
			ObjectifyService.ofy().delete().keys(partnerkeys).now();
			
			log.info("Partner Data deleted successfully");
			resp.setContentType("text/plain");
			resp.getWriter().println("{\"status\":\"Partner Data deleted successfully\"}");
		}
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doPost(req, resp);
	}

}