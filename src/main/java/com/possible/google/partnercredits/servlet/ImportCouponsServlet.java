package com.possible.google.partnercredits.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreInputStream;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.possible.google.partnercredits.PropertiesUtil;
import com.possible.google.partnercredits.entity.CouponData;
import com.possible.google.partnercredits.entity.CouponDataRecord;

import au.com.bytecode.opencsv.CSVReader;

@SuppressWarnings("serial")
public class ImportCouponsServlet extends HttpServlet {

	private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService(); 
	
	
	// Process the http POST of the form
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		PropertiesUtil props = new PropertiesUtil();
		String action = req.getParameter("action");
		String token = req.getParameter("token");
		
		if (token == null || (token != null && !token.equals(props.getProperty("security.token")))) {
			// DISPLAY
			resp.setContentType("text/plain");
			resp.getWriter().println("{'status':'Invalid Token'}");
		}else{
			if(action!=null && action.equals("view")){
				list(req,resp);
			}else{
				defaultAction(req, resp);	
			}
		}
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doPost(req, resp);
	}

	private void defaultAction(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		PrintWriter out = resp.getWriter();

		Map<String, List<BlobKey>> blobs = blobstoreService.getUploads(req);
		List<BlobKey> bkList = blobs.get("importCSV");
		BlobKey blobKey = bkList.get(0);
		BlobstoreInputStream blobStream = new BlobstoreInputStream(blobKey);
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(blobStream));

		// New CSVReader object reading from bufferedReader
		CSVReader csvReader = new CSVReader(bufferedReader);
		List<String[]> rows = csvReader.readAll();

		resp.getWriter().println("Uploading... ");
		
		String arr[];
		String coupon=null;
		int counter = 0;
		for (int i = 0; i < rows.size(); i++) {
			arr = rows.get(i);
			coupon = arr[0];
			
			if(coupon!=null){
				ObjectifyService.ofy().save().entity(new CouponData(coupon)).now();
				out.println(coupon);
				counter++;
			}
		}
		out.println("Uploaded " + counter + " out of " + rows.size() + " coupon codes.\n\n");
		
		list(req,resp);
		csvReader.close();
	}
	
	private void list(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		// RETRIEVE
		Key<CouponDataRecord> record = Key.create(CouponDataRecord.class, "coupondata");
		List<CouponData> records = ObjectifyService.ofy().load()
				.type(CouponData.class) 
				.ancestor(record) 
				.list();
		
		resp.getWriter().println("There are now a total of " + records.size() + " records.");
		
		// DISPLAY
		resp.setContentType("text/plain");
		if (!records.isEmpty()) {
			for (CouponData data : records) {
				resp.getWriter().println(
						data.code + "   " + data.isAvailable );
			}
		}
	}
}