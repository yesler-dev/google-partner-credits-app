package com.possible.google.partnercredits.servlet;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Work;
import com.possible.google.partnercredits.PropertiesUtil;
import com.possible.google.partnercredits.email.EmailHelper;
import com.possible.google.partnercredits.entity.Coupon;
import com.possible.google.partnercredits.entity.CouponData;
import com.possible.google.partnercredits.entity.CouponDataRecord;

@SuppressWarnings("serial")
public class AssignCouponServlet extends HttpServlet {
	
	private static Logger log = Logger.getLogger(AssignCouponServlet.class.getName());
	
	// Process the http POST of the form
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		String token = req.getParameter("token");
		PropertiesUtil props = new PropertiesUtil();
		if (token == null || (token != null && !token.equals(props.getProperty("security.token")))) {
			//Display the error message
			log.severe("Invalid token. " + token);
			resp.setContentType("text/plain");
			resp.getWriter().println("{\"status\":\"Invalid Token\"}");
		} else {
			defaultAction(req, resp);
		}
		
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doPost(req, resp);
	}

	private void defaultAction(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		PropertiesUtil props = new PropertiesUtil();
		// Retrieve an available coupon from coupondata datastore.
		final Key<CouponDataRecord> cdRecord = Key.create(CouponDataRecord.class, "coupondata");
		List<CouponData> cdList = ObjectifyService.ofy().cache(false).load()
				.type(CouponData.class).ancestor(cdRecord)
				.filter("isAvailable", true)
				// .limit(COUPON_THRESHOLD) // Only show 1 of them.
				.list();

		
		CouponData couponData = null;
		String couponCode = null;
		if (cdList != null && !cdList.isEmpty()) {
			couponData = cdList.get(0);

			if (couponData.isAvailable == true) {
				couponCode = couponData.code;
				log.info("####### COUPON CODE TO USE: " + couponCode);
				
				log.info(":::::TRANSACTION START");
				String result = ObjectifyService.ofy().transactNew (new Work<String>() {
					  public String run () {
						  String retValue=null;
						  List<CouponData> cdList2 = ObjectifyService.ofy().cache(false).load()
									.type(CouponData.class).ancestor(cdRecord)
									.filter("isAvailable", true)
									// .limit(COUPON_THRESHOLD) // Only show 1 of them.
									.list();
						  
						  if (cdList2 != null && !cdList2.isEmpty()) {
							  CouponData couponData2 = cdList2.get(0);
							  String couponCode2 = couponData2.code;
						  
							// Issue the available coupon. Save to coupon datastore
							Coupon coupon2 = new Coupon(couponCode2);
							ObjectifyService.ofy().save().entity(coupon2).now();
							log.info(":::::SAVED COUPON " + couponCode2);
	
							// Mark the coupon as already used.
							couponData2.setAvailable(false);
							ObjectifyService.ofy().save().entity(couponData2).now();
							log.info(":::::UPDATED COUPON DATA FLAG " + couponCode2);
							retValue =  "success:" + couponCode2;
							
						  }else{
							retValue = "fail:no more codes";
						  }
						  
						  
						  return retValue;
					  }
					});
				
				log.info(":::::TRANSACTION END " + result);
				String[] resultArr = result.split(":");
				if(resultArr[0].equals("success")){
					// Print out the JSon success response.
					resp.setContentType("text/plain");
					resp.getWriter().println(
							"{\"status\":\"success\",\"data\":{\"code\": \"" + resultArr[1] + "\"}}");

					// Send out email notification.
					int couponsLeft=cdList.size()-1;
					if (cdList != null && couponsLeft <= Integer.parseInt(props.getProperty("coupon.threshold"))) {
						EmailHelper.sendNotificationMail(couponsLeft);
					}
				}else{
					// Display the status response.
					log.warning("No more codes available.");
					resp.setContentType("text/plain");
					resp.getWriter().println("{\"status\":\"No more codes available\"}");
				}
				
			} else {
				// Display the status response.
				log.warning("No more codes available." + "");
				resp.setContentType("text/plain");
				resp.getWriter().println("{\"status\":\"No more codes available\"}");
			}
		} else {
			//Display the status response.
			log.warning("No more codes available.");
			resp.setContentType("text/plain");
			resp.getWriter().println("{\"status\":\"No more codes available\"}");
		}
	}
}