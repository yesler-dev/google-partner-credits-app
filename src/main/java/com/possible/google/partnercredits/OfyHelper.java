package com.possible.google.partnercredits;

import java.util.logging.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.googlecode.objectify.ObjectifyService;
import com.possible.google.partnercredits.entity.Coupon;
import com.possible.google.partnercredits.entity.CouponData;
import com.possible.google.partnercredits.entity.CouponDataRecord;
import com.possible.google.partnercredits.entity.CouponRecord;
import com.possible.google.partnercredits.entity.PartnerData;
import com.possible.google.partnercredits.entity.PartnerDataRecord;
import com.possible.google.partnercredits.servlet.ExportCouponsServlet;

/**
 * OfyHelper, a ServletContextListener, is setup in web.xml to run before a JSP
 * is run. This is required to let JSP's access Ofy.
 **/
public class OfyHelper implements ServletContextListener {
	
	private static Logger log = Logger.getLogger(ExportCouponsServlet.class.getName());
	
	public void contextInitialized(ServletContextEvent event) {
		// This will be invoked as part of a warmup request, or the first user
		// request if no warmup
		// request.
		ObjectifyService.register(CouponDataRecord.class);
		ObjectifyService.register(CouponRecord.class);
		ObjectifyService.register(Coupon.class);
		ObjectifyService.register(CouponData.class);
		ObjectifyService.register(PartnerData.class);
		ObjectifyService.register(PartnerDataRecord.class);
		
		
		log.info("Objects registered.");;

	}

	public void contextDestroyed(ServletContextEvent event) {
		// App Engine does not currently invoke this method.
	}
}