package com.possible.google.partnercredits.entity;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

@Entity
public class CouponData {
	@Parent Key<CouponDataRecord> record;
	@Id	public String code;
	@Index public boolean isAvailable;
	
	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}
	/**
	 * Simple constructor just sets the date
	 **/
	public CouponData() {
		isAvailable = true;
	}
	  /**
	   * Takes all important fields
	   **/
	  public CouponData(String code) {
	    this();
	    record = Key.create(CouponDataRecord.class, "coupondata");
	    this.code = code;
	  }	  
	  
	  public CouponData(String code, boolean isAvailable) {
	    record = Key.create(CouponDataRecord.class, "coupondata");
	    this.code = code;
	    this.isAvailable = false;
	  }	
}