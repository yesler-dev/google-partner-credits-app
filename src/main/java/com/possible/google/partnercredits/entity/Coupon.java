package com.possible.google.partnercredits.entity;

import java.util.Date;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

@Entity
public class Coupon {
	@Parent Key<CouponRecord> record;
	@Id	public String thisCode;
	public Date claimed_on;
	public Date requested_on;
	public Date updated_on;
	@Index public Date date; //create
	
	
	/**
	 * Simple constructor just sets the date
	 **/
	public Coupon() {
		date = new Date();
	}
	  /**
	   * Takes all important fields
	   **/
	  public Coupon(String code) {
	    this();
	    record = Key.create(CouponRecord.class, "coupon");
	    thisCode = code;
	    claimed_on = new Date();
	    requested_on = new Date();
	    updated_on = new Date();
	    date = new Date();
	  }	  
}