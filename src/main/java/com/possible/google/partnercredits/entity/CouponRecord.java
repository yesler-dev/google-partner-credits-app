package com.possible.google.partnercredits.entity;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
public class CouponRecord {
  @Id public String record;
}