package com.possible.google.partnercredits.email;

import javax.mail.PasswordAuthentication;



public class SMTPAuthenticator extends javax.mail.Authenticator {

	//private static Logger log = Logger.getLogger(SMTPAuthenticator.class.getName());
	
	private final String username;
	private final String password;
	
	public SMTPAuthenticator(String username, String password){
		this.username = username;
		this.password = password;
	}
	
	public PasswordAuthentication getPasswordAuthentication() {

		String username = this.username;
		String password = this.password;
		return new PasswordAuthentication(username, password);
	}

}
